
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <FS.h>
#include "SPIFFS.h"
#include <MFRC522.h> //biblioteca responsável pela comunicação com o módulo RFID-RC522
#include <SPI.h> //biblioteca para comunicação do barramento SPI

const char *ssid = "homerob2";
const char *password = "mestre5748";

File fsUploadFile;              // a File object to temporarily store the received file

String getContentType(String filename); // convert the file extension to the MIME type
//bool handleFileRead(String path, String checkID);       // send the right file to the client (if it exists)
void handleFileUpload();                // upload a new file to the SPIFFS
bool handleRoot(String fileName);

WebServer server(80);

//const int led = 13;

//Define variaveis para leitura Cartao
#define SS_PIN    21
#define RST_PIN   22

#define SIZE_BUFFER     18
#define MAX_SIZE_BLOCK  16

#define pinVerde     12
#define pinVermelho  32

//esse objeto 'chave' é utilizado para autenticação
MFRC522::MIFARE_Key key;
//código de status de retorno da autenticação
MFRC522::StatusCode status;

// Definicoes pino modulo RC522
MFRC522 mfrc522(SS_PIN, RST_PIN); 



bool handleRoot(String fileName) {
  //Serial.println(fileName);
//  digitalWrite(led, 1);
  char temp[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  if (getFileSize("teste.csv") != ""){
    Serial.println("File size: "+ getFileSize("teste.csv"));
  }
  else {
    Serial.println("Arquivo não encontrado");
  }

  String str = String("<!DOCTYPE HTML>");
  str += "<html style='background-color: #577159;'><meta http-equiv='Content-Language' content='pt-br' />";
  str +="<body><div align='center' class='table' id='table' >";

  str +="<div style='width: 600px; height: 400px; border:1px solid; background-color: #505F51;'>";
  str +="<form action='./upload' method='post' enctype='multipart/form-data'>";

  str +="<h1>Upload lista credenciais autorizada!</h1>";

  str +="<div align='center'> <div id='bordaFile' style='border:1px solid; background-color: #FFFFFF; width: 500px; height: 30;'><input type='file' name='fileToUpload' id='fileToUpload' style='width: 500px; height: 25px; font-size: 20px;'></div> <div/>";

  str +="<div align='center' style='padding: 15px;' > <input type='submit' value='Upload Arquivo' name='submit'> </div>";

  str +="</form></div>";

  str +="<div id='arquivo' style='width: 500px; height: 60px; padding: 5px;'>";

  str +="<div style='padding: 5px;'><table style='width: 100%; height: 60%;'><tr style='width: 400px;'>";

  if (getFileSize("teste.csv") != ""){
     str +="<td style='width: 50%; height: 100%;'> <div align='left'><font size='3' color='#FEFEFE'>Arquivo Atual: teste.csv</font></div></td>";

     str +="<td style='width: 50%; height: 100%;'> <div align='left'><font size='3' color='#FEFEFE'>Tamanho: "+getFileSize("teste.csv")+"bytes </font></div></td>";
  }else {
     str +="<td style='width: 50%; height: 100%;'> <div align='left'><font size='3' color='#FEFEFE'>Arquivo Atual:</font></div></td>";

     str +="<td style='width: 50%; height: 100%;'> <div align='left'><font size='3' color='#FEFEFE'>Tamanho:</font></div></td>";
  }
  
  str +="</tr></table></div></div>";

  str +="<div align='center' style='padding: 20px; width: 500px; height: 100px; background-color: #BF442A;'><h3>";
  str +="<p><font size='3' color='#FEFEFE'>Somente arquivos .csv sao aceitos para upload</font></p>";
  str +="<p><font size='3' color='#FEFEFE'>Os arquivos devem conter somente texto separado por \";\"</font></p>";

  str +="</h3></div></div></body></html>";

  server.send(200, "text/html", str);

  Serial.println("Free: "+String(getFreeSpace()));

  
//  digitalWrite(led, 0);
}

void handleNotFound() {
//  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
//  digitalWrite(led, 0);
}

String getFileSize(String namef){
    
   String dir = "/";
   String path = dir + namef;
   
   String sizef;
   
   if (SPIFFS.exists(path)){
      File file = SPIFFS.open(path, "r");                  // Open the file
      if (!file){
         return "Problem to open file!";
      }else {               
        
         sizef = file.size();     
         file.close();
         return sizef;
      }       
   }
    return "File not found!!";
  
}

int getFreeSpace(){
   int freeSize;
   int totalB;
   int usedB;
   
   totalB = SPIFFS.totalBytes();
   usedB = SPIFFS.usedBytes();
   freeSize = totalB - usedB;

   return freeSize; 
}


//Leitura usando RC522
bool checkID(String idCard, String namef){
   
   int card;
   String dir = "/";
   String path = dir + namef;
   uint8_t buffer[9];
   
   unsigned long time = 0;
   time = millis();

   if (SPIFFS.exists(path)){
      File file = SPIFFS.open(path, "r");                  // Open the file
      if (!file){
        return false;       
        file.close();
      }else{
        File file = SPIFFS.open(path, "r");
           
           while (file.available()) {
              
              int l = file.read(buffer, sizeof(buffer));
              buffer[l-1]= 0;
              char* filep = (char*) buffer;
              
              card = idCard.compareTo(filep);
              unsigned long time2 = millis() - time;
              //Serial.println(String(filep)+" - "+String(idCard));
              if (card == 0) { file.close(); Serial.print("Tempo necessário:");Serial.println(time2); return true;}

           }
            return false;
            
            file.close(); 
      }
   
   }else { return false; time = 0;}

}


//Leitura via porta serial para simulação
String leituraSerial()
{

  byte buffer[10]="";


    Serial.readBytesUntil('#', (char*)buffer, sizeof(buffer));
    int l = sizeof(buffer);
    buffer[l-1]= 0;
    return String((char *)buffer);
}


void handleFileUpload() { // upload a new file to the SPIFFS
  HTTPUpload& upload = server.upload();
  if (upload.status == UPLOAD_FILE_START) {
    String filename = upload.filename;
    if (!filename.startsWith("/")) filename = "/" + filename;
    Serial.print("handleFileUpload Name: "); Serial.println(filename);

    fsUploadFile = SPIFFS.open(filename, "w");            // Open the file for writing in SPIFFS (create if it doesn't exist)
    filename = String();
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    if (fsUploadFile)
      fsUploadFile.write(upload.buf, upload.currentSize); // Write the received bytes to the file
  } else if (upload.status == UPLOAD_FILE_END) {
    if (fsUploadFile) {                                   // If the file was successfully created
      fsUploadFile.close();                               // Close the file again
      Serial.print("handleFileUpload Size: "); Serial.println(upload.totalSize);
      
      server.sendHeader("Location", "/upload");     // Redirect the client to the success page
      server.send(303);

    } else {
      server.send(500, "text/plain", "500: couldn't create file");
    }
  }

}


void setup(void) {
//  pinMode(led, OUTPUT);
//  digitalWrite(led, 0);
  Serial.begin(115200);

  SPI.begin(); // Init SPI bus

  pinMode(pinVerde, OUTPUT);
  pinMode(pinVermelho, OUTPUT);
  
  // Inicia MFRC522
  mfrc522.PCD_Init(); 
  
  WiFi.mode(WIFI_STA);
  char hostname[8];
  
  String hostmac = String(WiFi.macAddress());
 
  String HOSTNAME = "esp"+ String(hostmac.substring(12,14)) + String(hostmac.substring(15,17)); //get the last four variables from MAC to hostname

  Serial.println(HOSTNAME);
  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE); //necessary to change hostname
  

  WiFi.setHostname(HOSTNAME.c_str()); //.c_str() convert String to char
  
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection

  int countWait = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    countWait += 1;
    Serial.print(".");
    if (countWait >= 30) { Serial.println("Reboot!"); delay(1000); ESP.restart(); }
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());


  if (MDNS.begin(HOSTNAME.c_str())) {
    Serial.println("MDNS responder started");
//    Heltec.display->drawString(0, 20, "MDNS responder started");
  }

  SPIFFS.begin();

  server.on("/", []() { handleRoot("OK");} );


  server.on("/upload", HTTP_POST,                       // if the client posts to the upload page
  []() {
    server.send(200);
  },                          // Send status 200 (OK) to tell the client we are ready to receive
  handleFileUpload                                    // Receive and save the file
           );


  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/upload", HTTP_GET, []() {
    server.send(200, "text/plain", "successfully file uploaded!");
  });
  
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");

  // Mensagens iniciais no serial monitor
  Serial.println("Aproxime o seu cartao do leitor...");
  Serial.println();


  //cria uma tarefa que será executada na função coreTaskZero, com prioridade 1 e execução no núcleo 0
  //coreTaskZero: piscar LED e contar quantas vezes

// xTaskCreatePinnedToCore(
//  leituraDados,   //Função que será executada
//  "leituraDados", //Nome da tarefa
//  10000,     //Tamanho da pilha
//  NULL,      //Parâmetro da tarefa (no caso não usamos)
//  3,         //Prioridade da tarefa
//  NULL,      //Caso queira manter uma referência para a tarefa que vai ser criada (no caso não precisamos)
//  0);        //Número do core que será executada a tarefa (usamos o core 0 para o loop ficar livre com o core 1)
   
//delay(500);

}

void loop(void) {

   server.handleClient();
//   String leia = leituraSerial();
   
   
//   if (leia != "" && leia != "\n") {
//      bool statusRead = checkID(leia, "teste.csv");
//      Serial.println(statusRead);
//   }


  //Serial.println("Iniciando!!");
   // Aguarda a aproximacao do cartao
  if ( ! mfrc522.PICC_IsNewCardPresent())  
  {
    
    return;
  }
  // Seleciona um dos cartoes
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }

    // Dump debug info about the card; PICC_HaltA() is automatically called
  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

   // verifica se ainda está com o cartão/tag
//  if ( ! mfrc522.PICC_IsNewCardPresent()) 
//  {
//    return;
//  }
  

 leituraDados();
 
 
  // instrui o PICC quando no estado ACTIVE a ir para um estado de "parada"
  mfrc522.PICC_HaltA(); 
  // "stop" a encriptação do PCD, deve ser chamado após a comunicação com autenticação, caso contrário novas comunicações não poderão ser iniciadas
  mfrc522.PCD_StopCrypto1();


}


void leituraDados()
{
  //imprime os detalhes tecnicos do cartão/tag
  //mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); 

  //Prepara a chave - todas as chaves estão configuradas para FFFFFFFFFFFFh (Padrão de fábrica).
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  //buffer para colocar os dados ligos
  byte buffer[SIZE_BUFFER] = {0};

  //bloco que faremos a operação
  byte bloco = 1;
  byte tamanho = SIZE_BUFFER;


  //faz a autenticação do bloco que vamos operar
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, bloco, &key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    digitalWrite(pinVermelho, HIGH);
    delay(200);
    digitalWrite(pinVermelho, LOW);
    return;
  }

  //faz a leitura dos dados do bloco
  status = mfrc522.MIFARE_Read(bloco, buffer, &tamanho);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    digitalWrite(pinVermelho, HIGH);
    delay(200);
    digitalWrite(pinVermelho, LOW);
    return;
  }
  else{
      digitalWrite(pinVerde, HIGH);
      delay(200);
      digitalWrite(pinVerde, LOW);
  }


String UID_HEX="";

//Serial.print("Printing HEX UID : ");
for (byte i = 0; i < mfrc522.uid.size; i++) {
String partial_id_HEX = String(mfrc522.uid.uidByte[i], HEX);
UID_HEX += partial_id_HEX;
}

UID_HEX.toUpperCase();

bool access = checkID(UID_HEX, "teste.csv");


//Serial.print("Full ID Hex : ");
//Serial.println(UID_HEX);

if (!access) {
  Serial.println("Acesso não permitido");
}else{
  Serial.println("Acesso permitido");
}

  //Serial.print(F("\nDados bloco ["));
  //Serial.print(bloco);Serial.print(F("]: "));

  //imprime os dados lidos
  //for (uint8_t i = 0; i < MAX_SIZE_BLOCK; i++)
  //{
  //    Serial.write(buffer[i]);
  //}
  Serial.println(" ");
}
